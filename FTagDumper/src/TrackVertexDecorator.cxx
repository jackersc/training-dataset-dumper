#include "TrackVertexDecorator.hh"


// the constructor just builds the decorator
TrackVertexDecorator::TrackVertexDecorator(const std::string& prefix):
  m_track_sv1_idx(prefix + "SV1VertexIndex"),
  m_track_jf_idx(prefix + "JFVertexIndex"),
  m_track_vsi_idx(prefix + "VSIVertexIndex"),
  m_SV1VxAccessor("SV1_vertices"),
  m_jfVxAccessor("JetFitter_JFvertices"),
  m_AMVFVertices("TTVA_AMVFVertices"),
  m_AMVFWeights("TTVA_AMVFWeights"),
  m_AMVFWeightPV("AMVFWeightPV")
{
}


void TrackVertexDecorator::decorate_sv1_index( const std::vector<ElementLink<xAOD::VertexContainer>>& sv1_vertices ) const {
  
  // loop over vertices
  for (unsigned int sv1_idx = 0; sv1_idx < sv1_vertices.size(); sv1_idx++) { 
    if (!sv1_vertices.at(sv1_idx).isValid()) {
      continue;
    }

    // jet tracks used in this vertex
    const xAOD::Vertex *tmpVertex = *(sv1_vertices.at(sv1_idx));
    const std::vector< ElementLink<xAOD::TrackParticleContainer>> tracks_in_vertices = tmpVertex->trackParticleLinks();

    // decorate tracks
    for ( auto track : tracks_in_vertices ) {
      m_track_sv1_idx(*(*track))  = sv1_idx;
    }
  }
}

void TrackVertexDecorator::decorate_vsi_index( const xAOD::VertexContainer& vsi_vertices ) const {
  
  // loop over vertices
  for (  const xAOD::Vertex* tmpVertex : vsi_vertices) { 

    size_t vsi_idx = tmpVertex->index();

    // jet tracks used in this vertex
    const std::vector< ElementLink<xAOD::TrackParticleContainer>> tracks_in_vertices = tmpVertex->trackParticleLinks();

    // decorate tracks
    for ( auto track : tracks_in_vertices ) {
      m_track_vsi_idx(*(*track))  = vsi_idx;
    }
  }
}

void TrackVertexDecorator::decorate_jf_index( const std::vector<ElementLink<xAOD::BTagVertexContainer>>& jf_vertices ) const {
  
  // loop over vertices
  for (unsigned int jf_idx = 0; jf_idx < jf_vertices.size(); jf_idx++) { 
    if (!jf_vertices.at(jf_idx).isValid()) {
      continue;
    }

    // jet tracks used in this vertex
    const xAOD::BTagVertex *tmpVertex = *(jf_vertices.at(jf_idx));
    const std::vector< ElementLink<xAOD::TrackParticleContainer>> tracks_in_vertices = tmpVertex->track_links();

    // decorate tracks
    for ( auto track : tracks_in_vertices ) {
      m_track_jf_idx(*(*track))  = jf_idx;
    }
  }
}

float TrackVertexDecorator::get_AMVF_PV_weight( const xAOD::TrackParticle& track, 
                          const xAOD::Vertex& pv ) const {
  // accessors
  auto TTVA_AMVFVertices = m_AMVFVertices(track);
  auto TTVA_AMVFWeights  = m_AMVFWeights(track);
  
  // for each AMVF vertex
  for (unsigned int vtx_idx = 0; vtx_idx < TTVA_AMVFVertices.size(); vtx_idx++) { 

    if ( !TTVA_AMVFVertices.at(vtx_idx).isValid() ) {
      continue;
    }

    auto vertex = *(TTVA_AMVFVertices.at(vtx_idx));

    // is this the PV?
    if ( vertex == &pv ) {

      // return track weighting to the PV
      return TTVA_AMVFWeights.at(vtx_idx);
    }
  }

  // track must not have been used in the PV fit
  return 0.0;
}

void TrackVertexDecorator::decorateTracksWithVSI(TrackSelector::Tracks tracks, const xAOD::VertexContainer& vsi_vertices) const {
   for ( const auto& track : tracks ) {
     // default vertex index
     m_track_vsi_idx(*track) = -2;
     decorate_vsi_index(vsi_vertices);
   }
}
						
// this call actually does the work on the track
void TrackVertexDecorator::decorateAll(TrackSelector::Tracks tracks,
				       const xAOD::BTagging& btag,
                                       const xAOD::Vertex& pv) const {

  
  for ( const auto& track : tracks ) {

    // add track PV matching weighting
    m_AMVFWeightPV(*track) = get_AMVF_PV_weight(*track, pv);

    // default vertex index
    m_track_sv1_idx(*track) = -2;
    m_track_jf_idx(*track)  = -2;
  }

  // get vertex collections
  std::vector<ElementLink<xAOD::VertexContainer>> sv1_vertices = m_SV1VxAccessor(btag);
  std::vector<ElementLink<xAOD::BTagVertexContainer>> jf_vertices = m_jfVxAccessor(btag);

  // decorate tracks with vertex indices
  decorate_sv1_index(sv1_vertices);
  decorate_jf_index(jf_vertices);
}

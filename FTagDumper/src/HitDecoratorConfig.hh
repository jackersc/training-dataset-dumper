#ifndef HIT_DECORATOR_CONFIG
#define HIT_DECORATOR_CONFIG

struct HitDecoratorConfig {
  float dR_hit_to_jet;
  bool save_endcap_hits;
  bool use_splitProbability;
};

#endif
